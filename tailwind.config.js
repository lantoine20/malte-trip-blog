const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        "stunning-red": "#c8313f",
      },
    },
    fontFamily: {
      heading: ["Dongle", ...defaultTheme.fontFamily.sans],
    },
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: [
      {
        light: {
          primary: "#374151",

          secondary: "#F000B8",

          accent: "#37CDBE",

          neutral: "#3D4451",

          "base-100": "#FFFFFF",

          info: "#3ABFF8",

          success: "#36D399",

          warning: "#FBBD23",

          error: "#F87272",
        },
        dark: {
        primary: "#FFFFFF",
    "primary-content": "#ffffff",
    secondary: "#D926AA",
    "secondary-content": "#ffffff",
    accent: "#1FB2A5",
    "accent-content": "#ffffff",
    neutral: "#191D24",
    "neutral-focus": "#111318",
    "neutral-content": "#A6ADBB",
    "base-100": "#2A303C",
    "base-200": "#242933",
    "base-300": "#20252E",
    "base-content": "#A6ADBB",
      },
      },
      ,
      "cupcake",
      "luxury",
      "lemonade",
      "pastel",
    ],
  },
};
