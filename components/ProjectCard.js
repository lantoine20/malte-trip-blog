/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import Moment from "react-moment";
import React, { useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";

function ProjectCard(props) {
  useEffect(() => {
    AOS.init({ duration: 2000 });
  }, []);

  return (
    <Link href={`/blogs/${props.project.nom}`} key={props.project.nom}>
      <div
        className="relative flex flex-col shadow-lg rounded-2xl bg-base-100 hover:cursor-pointer"
        data-aos="fade-up"
        data-aos-duration="10000"
      >
        <img
          src={props.project.image}
          className="rounded-t-2xl w-auto lg:h-48 md:h-48 sm:h-48"
        ></img>
        <span className="badge badge-primary badge-lg  text-base-100 absolute left-2 top-2">
          {props.project.categorie}
        </span>
        <p className="font-sans text-primary mb-3 mt-3 mr-4 ml-4 text-xl font-bold leading-none sm:text-2xl ">
          {props.project.nom}
        </p>
        <p className="font-sans  text-md mb-6 ml-4 mr-4">
          {props.project.description}
        </p>
        <br />
        <div className="absolute bottom-0 left-0">
          <p className=" font-sans text-sm  ml-4 mb-3 mr-4 mt-4">
            <Moment format="DD/MM/YYYY HH:mm">{props.project.date}</Moment>
          </p>
        </div>
      </div>
    </Link>
  );
}

export default ProjectCard;
