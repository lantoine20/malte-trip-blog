import { FaFacebook, FaGithub, FaGoogle } from "react-icons/fa";
import { useEffect } from "react";

export const Footer = () => {
  return (
    <footer className="footer footer-center p-10 bg-gradient-to-t from-base-300 via-base-100 to-base-100 text-base-content rounded">
      <div>
        <div className="grid grid-flow-col gap-4">
          <a
            className="text-xl text-content rounded-full"
            href="https://www.facebook.com/gautier.lantoine"
          >
            <FaFacebook />
          </a>
          <a
            className="text-xl text-content rounded-full "
            href="https://gitlab.com/lantoine20/"
          >
            <FaGithub />
          </a>

          <a
            className="text-xl text-content rounded-full"
            href={
              "https://mail.google.com/mail/?view=cm&fs=1&to=lantoine.gaut@gmail.com"
            }
          >
            <FaGoogle />
          </a>
        </div>
      </div>
      <div>
        <p>Copyright © 2022 - All rights reserved by GL Prod™</p>
      </div>
    </footer>
  );
};
