import { useEffect } from "react";
import { themeChange } from "theme-change";
import Link from "next/link";

function Navbar() {
  useEffect(() => {
    themeChange(false);
    // 👆 false parameter is required for react project
  }, []);

  return (
    <div className=" md:flex p-6 justify-center mt-2 ">
      <div className="navbar bg-base-100 shadow-md rounded-2xl">
        <div className="navbar-start">
          <div className="dropdown">
            <label tabIndex="0" className="btn btn-ghost lg:hidden">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 6h16M4 12h8m-8 6h16"
                />
              </svg>
            </label>
            <ul
              tabIndex="0"
              className="menu menu-compact dropdown-content  mt-3 p-2 shadow bg-base-100 rounded-box w-52"
            >
              <Link href="/">
                <li>
                  <p className="text-xl text-primary">Accueil</p>
                </li>
              </Link>

              <Link href="/#blogs">
                <li>
                  <p className="text-xl text-primary">Articles</p>
                </li>
              </Link>
              <Link href="/#gallerie">
                <li>
                  <p className="text-xl text-primary">Galerie</p>
                </li>
              </Link>
            </ul>
          </div>
          <Link href="/">
            <p className="btn btn-ghost normal-case text-xl text-primary">
              GautTrip
            </p>
          </Link>
        </div>
        <div className="navbar-center hidden lg:flex">
          <ul className="menu menu-horizontal text-xl p-1">
            <Link href="/">
              <li>
                <p className="text-primary">Accueil</p>
              </li>
            </Link>

            <Link href="/#blogs">
              <li>
                <p className="text-primary">Articles</p>
              </li>
            </Link>
            <Link href="/#gallerie">
              <li>
                <p className="text-primary">Galerie</p>
              </li>
            </Link>
          </ul>
        </div>
        <div className="navbar-end">
          <form-control>
            <select
              data-choose-theme
              className="select select-primary w-full max-w-xs text-primary"
            >
              <option disabled selected>
                Theme
              </option>
              <option value="light" className="text-primary">
                Light
              </option>
              <option value="dark" className="text-primary">
                Dark
              </option>
              <option value="cupcake" className="text-primary">
                Cupcake
              </option>
              <option value="luxury" className="text-primary">
                Luxury
              </option>
              <option value="lemonade" className="text-primary">
                Lemonade
              </option>
              <option value="pastel" className="text-primary">
                Pastel
              </option>
            </select>
          </form-control>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
