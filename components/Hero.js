/* eslint-disable @next/next/no-img-element */
import Typical from "react-typical";
import Link from "next/link";
import Navbar from "./Navbar";
import DrawerContent from "./DrawerContent";
import { FaInfo } from "react-icons/fa";
import { useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";

function Hero() {
  useEffect(() => {
    AOS.init({ duration: 2000 });
  }, []);

  return (
    <div className="relative drawer h-screen drawer-end">
      <input id="my-drawer-4" type="checkbox" className="drawer-toggle"></input>
      <div className="drawer-content">
        <div className="font-mono overflow-hidden bg-gradient-to-b from-base-300 via-base-100 to-base-100 flex flex-col">
          <Navbar />

          <div className="container m-auto flex flex-col justify-center items-center ">
            <div data-aos="fade-up" data-aos-duration="10000">
              <img
                alt=""
                src="/assets/memoji.png"
                className="mx-auto mb-4 xl:mb-8"
              />

              <p className="text-center text-primary font-heading lg:text-7xl md:text-7xl sm:text-7xl text-5xl">
                Salut c&apos;est Gaut !
              </p>
              <h2 className="max-w-3xl text-5xl md:text-5xl sm:text-4xl mx-auto text-primary text-center font-heading">
                <Typical
                  steps={[
                    "Suis mon séjour ici",
                    2000,
                    "Découvre mes aventures",
                    2000,
                  ]}
                  loop={Infinity}
                  wrapper="p"
                />
              </h2>
            </div>
            <label
              htmlFor="my-drawer-4"
              className="drawer-button bg-base-300 border border-primary p-3 rounded-l-xl text-primary absolute right-0  hover:cursor-pointer"
            >
              <FaInfo className="text-primaryt" />
            </label>
            <div className="flex items-center justify-center my-6">
              <Link href="#blogs">
                <a className="py-4 px-6 bg-transparent ring-primary hover:bg-base-100 hover:text-secondary text-primary text-xl text-center transition ease-in duration-200 font-semibold outline-none ring rounded-full shadow-lg">
                  Vers les articles !
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
      <div className="drawer-side">
        <label htmlFor="my-drawer-4" className="drawer-overlay"></label>
        <DrawerContent />
      </div>
    </div>
  );
}

export default Hero;
