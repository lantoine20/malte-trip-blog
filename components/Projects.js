import ProjectCard from "./ProjectCard";
import React, { useState, useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";

function Projects(props) {
  const [filter, setFilter] = useState("All");

  function buildProjects() {
    return props.projects.map((project) => {
      if (filter == "All") {
        return <ProjectCard project={project} key={project.id} />;
      } else {
        if (filter == project.categorie)
          return <ProjectCard project={project} key={project.id} />;
        else return null;
      }
    });
  }

  function buildDropdown() {
    const res = [];
    if (typeof props !== undefined) {
      props.projects.map((project) => {
        if (!res.includes(project.categorie)) {
          res.push(project.categorie);
        }
      });
      return res.map((item) => {
        return (
          <option value={item} key={item}>
            {item + "s"}
          </option>
        );
      });
    } else return <div></div>;
  }

  useEffect(() => {
    AOS.init({ duration: 2000 });
  }, []);

  return (
    <div
      id="blogs"
      className="px-6 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen md:px-24 lg:px-40 lg:py-20 h-full bg-gradient-to-t from-base-300 via-base-100 to-base-100"
    >
      <h2 className="text-center my-4 font-heading font-sans text-6xl sm:text-6xl">
        <span className="text-center text-primary self-center mb-2">
          Articles
        </span>
      </h2>
      <div className="flex flex-col m-2 items-center">
        <select
          className="select select-bordered m-2 w-full max-w-xs"
          onChange={(e) => setFilter(e.target.value)}
        >
          <option disabled selected value="All">
            Choisir une catégorie
          </option>
          <option value="All">Tous</option>
          {buildDropdown()}
        </select>
      </div>

      <div className="grid gap-8 row-gap-5 mb-8 lg:grid-cols-3 lg:row-gap-8 ">
        {buildProjects()}
      </div>
    </div>
  );
}

export default Projects;
