import { FaPlus } from "react-icons/fa";
import { useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";

function Gallerie(props) {
  function buildGallerie() {
    return props.gallerie.map((project, i) => {
      return (
        <div className="tooltip" key={i} data-tip="Clique !">
          {" "}
          <label htmlFor={"my-modal-" + i} className="hover:cursor-pointer">
            <img src={project.image} className="w-24 h-24 rounded-xl " />
          </label>
          <input
            type="checkbox"
            id={"my-modal-" + i}
            className="modal-toggle"
          />
          <div className="modal modal-bottom sm:modal-middle ">
            <div className="modal-box">
              <img src={project.image} className="w-full rounded-xl" />
              <div className="modal-action flex flex-col items-center">
                <label
                  htmlFor={"my-modal-" + i}
                  className="hover:cursor-pointer border border-red-500 text-red-500 rounded-full p-2"
                >
                  <FaPlus className="rotate-45 " />
                </label>
              </div>
            </div>
          </div>
        </div>
      );
    });
  }

  useEffect(() => {
    AOS.init({ duration: 2000 });
  }, []);

  return (
    <div
      id="gallerie"
      className="px-6 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen md:px-24 lg:px-40 lg:py-20 h-full bg-gradient-to-t from-base-100 via-base-100 to-base-300"
    >
      <h2 className="text-center my-4 font-heading font-sans text-6xl sm:text-6xl">
        <span className="text-center text-primary self-center mb-2">
          Galerie
        </span>
        <div className="dropdown dropdown-end">
          <label
            tabIndex="0"
            className="btn btn-circle btn-ghost btn-xs text-info"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              className="w-4 h-4 stroke-current"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
              ></path>
            </svg>
          </label>
          <div
            tabIndex="0"
            className="card compact dropdown-content shadow bg-base-100 rounded-box w-64"
          >
            <div className="card-body text-lg">
              <h2 className="card-title text-3xl">
                Clique sur les photos pour les voir en plus grandes 😁
              </h2>
            </div>
          </div>
        </div>
      </h2>
      <div className="flex justify-center items-center">
        <div className="grid grid-cols-3 gap-2 row-gap-3 mb-8 lg:grid-cols-8 lg:row-gap-8 ">
          {buildGallerie()}
        </div>
      </div>
    </div>
  );
}

export default Gallerie;
