import React, { useEffect, useState } from "react";

function DrawerContent(props) {
  const [lat, setLat] = useState([]);
  const [long, setLong] = useState([]);
  const [data, setData] = useState([]);

  const refresh = () => {
    window.location.href = "/";
  };

  useEffect(() => {
    const fetchData = async () => {
      await fetch(
        `https://api.openweathermap.org/data/2.5/weather/?lat=36.055000&lon=14.204167&units=metric&APPID=10c216c2c66c33965bf80e4608080432
        `
      )
        .then((res) => res.json())
        .then((result) => {
          setData(result);
        });
    };
    fetchData();
  }, [lat, long]);
  return (
    <div className="stats flex flex-col stats-vertical h-full shadow text-center">
      <div className="stat text-center">
        <div
          className="btn btn-accent-content mb-8"
          onClick={() => {
            if (typeof window !== "undefined") {
              refresh();
            }
          }}
        >
          Refresh
        </div>
        <div className="stat-title m-2">Date et heure à Malte</div>
        <div className="stat-value m-2">
          {new Date().toLocaleString(undefined, {
            hour: "2-digit",
            minute: "2-digit",
          })}
        </div>
        <div className="stat-value text-sm m-2">
          {new Date().toLocaleString(undefined, {
            day: "numeric",
            month: "numeric",
            year: "numeric",
          })}
        </div>
      </div>

      {typeof data.main != "undefined" ? (
        <div className="stat">
          <div className="stat-title mt-6">Météo à Malte</div>
          <div className="stat-value m-2 mx-auto">
            <img
              src={
                "http://openweathermap.org/img/wn/" +
                data.weather[0].icon +
                "@2x.png"
              }
            ></img>
            {parseInt(data.main.temp) + " °C"}
          </div>
          <div className="stat-desc">Humidité : {data.main.humidity} %</div>
          <div className="stat-desc">Pression : {data.main.pressure} hPa</div>
          <div className="stat-desc">
            Vent : {parseInt(data.wind.speed * 1.852)} km/h
          </div>
        </div>
      ) : (
        <div></div>
      )}
      <div className="stat text-center">
        <div className="stat-title m-2">Informations importantes</div>
        <div className="stat-value m-2">Dates</div>
        <div className="stat-desc m-2">26 juin 2022 au 23 août 2022</div>
        <div className="stat-value m-2">Adresse</div>
        <div className="stat-desc m-2">Ta’Lorenzo B&B Farmhouse7</div>
        <div className="stat-desc">
          L’ghaxra ta’ Awwissu San Lawrenz Gozo, Malte
        </div>
      </div>
    </div>
  );
}

export default DrawerContent;
