import getProjects from "../api/projects";
import getProject from "../api/project";
import Navbar from "../../components/Navbar";
import { useRouter } from "next/router";
import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";

const ProductPage = (props) => {
  useEffect(() => {
    AOS.init({ duration: 2000 });
  }, []);
  const router = useRouter();
  if (router.isFallback) {
    return <div>Loading product...</div>;
  }
  const contents = props.project.content.split("#");
  return (
    <div className="font-mono overflow-hidden bg-gradient-to-b from-base-300 via-base-100 to-base-100 min-h-screen h-auto flex flex-col">
      <Navbar />
      <div className="md:flex justify-center">
        <header className="w-full bg-transparent backdrop-filter items-center h-80 z-40 lg:hidden">
          <div className="relative z-20 flex flex-col justify-center h-full mx-auto flex-center">
            <div className="relative items-center flex w-full sm:pr-2 sm:ml-0 justify-center">
              <div
                id="left"
                className="flex flex-row items-center justify-center p-6"
              >
                <img
                  alt=""
                  src={props.project.image}
                  className="mx-auto w-screen lg:w-screen-1/2 md:w-screen-1/2 h-80 rounded-3xl"
                />
              </div>
            </div>
          </div>
        </header>
      </div>
      <div className="container mt-6 mx-auto flex flex-col h-max justify-center items-center ">
        <p className="text-center text-primary font-heading lg:text-5xl md:text-5xl sm:text-5xl text-5xl mx-4">
          {props.project.nom}
        </p>
        <h2 className="max-w-3xl my-6 text-xl m-auto md:text-2xl sm:text-2xl mx-4 text-center">
          {props.project.description}
        </h2>
        <div className="mockup-window border border-base-300 bg-base-300 m-4">
          <div className="flex justify-center px-4 py-4 bg-base-100">
            {" "}
            <div className="items-center justify-center my-6 p-4 rounded-xl text-center m-4 bg-transparent lg:w-2/3 md:w-2/3 ">
              {contents.map((content, i) => {
                if (i % 2 == 0) {
                  return (
                    <p
                      className="text-2xl p-2 text-primary text-justify"
                      data-aos="fade-up"
                      data-aos-duration="5000"
                    >
                      {content}
                    </p>
                  );
                } else {
                  return (
                    <p
                      className="text-lg p-2 text-justify text-content"
                      data-aos="fade-up"
                      data-aos-duration="5000"
                    >
                      {content}
                    </p>
                  );
                }
              })}
            </div>
          </div>
        </div>

        <div className="flex p-6"></div>
      </div>
    </div>
  );
};

export async function getStaticProps({ params }) {
  const project = await getProject(params.nom);
  return {
    props: {
      project,
    },
  };
}

export async function getStaticPaths() {
  const blogs = await getProjects();
  return {
    paths: blogs.map((_product) => {
      return {
        params: { nom: _product.nom },
      };
    }),
    fallback: true,
  };
}

export default ProductPage;
