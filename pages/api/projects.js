require("dotenv").config();
// the following lines are required to initialize a Notion client
const { Client } = require("@notionhq/client");
// this line initializes the Notion Client using our key
const notion = new Client({
  auth: "secret_9LVZL4XZRd5IebSqXBy0DYyzuM07L6BtqT9yW5P5k89",
});
const databaseId = process.env.NOTION_API_DATABASE;

export default async function getProjects() {
  const response = await notion.databases.query({
    database_id: databaseId,
  });

  const responseResults = response.results.map((page) => {
    return {
      id: page.id,
      nom: page.properties.Nom.title[0]?.plain_text,
      categorie: page.properties.Categorie.rich_text[0].plain_text,
      description: page.properties.Description.rich_text[0].plain_text,
      image: page.properties.Image.url,
      content: page.properties.Content.rich_text[0].plain_text,
      date: page.properties.Date.created_time,
    };
  });
  return responseResults;
}
