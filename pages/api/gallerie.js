require("dotenv").config();
// the following lines are required to initialize a Notion client
const { Client } = require("@notionhq/client");
// this line initializes the Notion Client using our key
const notion = new Client({
  auth: "secret_9LVZL4XZRd5IebSqXBy0DYyzuM07L6BtqT9yW5P5k89",
});
const databaseId = "c4577ddac12941baa45c0c8ee44e49f0";

export default async function getGallerie() {
  const response = await notion.databases.query({
    database_id: databaseId,
  });

  const responseResults = response.results.map((page) => {
    return {
      image: page.properties.Image.url,
    };
  });
  return responseResults;
}
