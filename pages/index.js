/* eslint-disable @next/next/google-font-display */
/* eslint-disable @next/next/no-page-custom-font */
import Head from "next/head";
import Hero from "../components/Hero";
import Projects from "../components/Projects";
import getProjects from "./api/projects";
import Gallerie from "../components/Gallerie";
import getGallerie from "./api/gallerie";
import { Footer } from "../components/Footer";
import "aos/dist/aos.css";

function Home({ projects, gallerie }) {
  return (
    <div id="Home">
      <Head>
        <link
          href="https://fonts.googleapis.com/css2?family=Dongle:wght@300&display=swap"
          rel="stylesheet"
        />
        <link rel="shortcut icon" href="/memoji.ico" />
        <link
          href="https://unpkg.com/aos@2.3.1/dist/aos.css"
          rel="stylesheet"
        />
      </Head>
      <Hero />
      <Projects projects={projects} />
      <Gallerie gallerie={gallerie} />
      <Footer />
    </div>
  );
}

export async function getStaticProps(context) {
  const projects = await getProjects();
  const gallerie = await getGallerie();
  return {
    props: {
      projects,
      gallerie,
    },
  };
}

export default Home;
